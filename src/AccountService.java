import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AccountService {
    private final AccountDao accountDao;

    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public void fillAccounts() {
        List<Account> accounts = new ArrayList<>();
        try {
            List<String> lines = Files.readAllLines(Paths.get("account-base.txt"));
            for (String line : lines) {
                String[] array = line.split(" ");
                accounts.add(new Account(new Card(array[0], array[1], array[2].equalsIgnoreCase("Active") ? CardStatus.ACTIVE : CardStatus.BLOCKED), Integer.parseInt(array[3])));
            }
            accountDao.fillAccountDao(accounts);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeAccounts() {
        List<String> lines = new ArrayList<>();
        for (Account ac : accountDao.getAllAccounts()) {
            lines.add(ac.getCard().getNumber() + " " + ac.getCard().getPin() + " " + ac.getCard().getStatus() + " " + ac.getBalance());
        }
        try {
            Files.write(Paths.get("account-base.txt"), lines, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void checkBalance(Card card) {
        System.out.println("Your balance: " + accountDao.getAccountByCard(card).getBalance() + "\n");
    }

    public long getMoneyFromAtm(BufferedReader br, Card card, long atmTotalCash) {
        Account account = accountDao.getAccountByCard(card);
        System.out.println("Cash advance.");
        boolean isRead = false;
        while (!isRead) {
            try {
                System.out.println("Enter the amount to withdraw from the account:\n");
                int amount = Integer.parseInt(br.readLine());
                if (atmTotalCash >= amount) {
                    if (account.getBalance() >= amount) {
                        atmTotalCash = atmTotalCash - amount;
                        account.setBalance(account.getBalance() - amount);
                        System.out.println("Operation approved.\nTake your money, please.\n");
                    } else {
                        System.out.println("Not enough money in the account. Choose another amount.\n");
                    }
                } else {
                    System.out.println("Not enough money in the ATM. Choose another amount.\n");
                }
                isRead = true;
            } catch (IOException | NumberFormatException e) {
                System.out.println("Incorrect entry, try again. Enter the amount:\n");
            }
        }
        return atmTotalCash;
    }

    public long addMoneyToAtm(BufferedReader br, Card card, long atmTotalCash) {
        System.out.println("Cash acceptance.");
        Account account = accountDao.getAccountByCard(card);
        boolean isRead = false;
        while (!isRead) {
            try {
                System.out.println("Enter the amount to replenish the balance:\n");
                int amount = Integer.parseInt(br.readLine());
                if (amount <= 1000000) {
                    atmTotalCash = atmTotalCash + amount;
                    account.setBalance(account.getBalance() + amount);
                    System.out.println("Operation completed successfully.\n");
                } else {
                    System.out.println("Operation failed. The maximum amount must not exceed 1000000.\n");
                }
                isRead = true;
            } catch (IOException | NumberFormatException e) {
                System.out.println("Incorrect entry, try again. Enter the amount:\n");
            }
        }
        return atmTotalCash;
    }

    public Card findCardByCardNumber(String cardNumber) {
        return accountDao.getAllAccounts().stream().filter(account -> account.getCard().getNumber().equals(cardNumber)).findAny().get().getCard();
    }
}
