import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AtmStarter {
    private CardValidator cardValidator;
    private Terminal terminal;

    public AtmStarter(CardValidator cardValidator, Terminal terminal) {
        this.cardValidator = cardValidator;
        this.terminal = terminal;
    }

    public void start() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        terminal.startProcessingUserRequests(br, cardValidator.startCheckingCard(br));
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}