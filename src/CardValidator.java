import java.io.BufferedReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CardValidator {
    private AccountService accountService;

    public CardValidator(AccountService accountService) {
        this.accountService = accountService;
    }

    public Card startCheckingCard(BufferedReader br) {
        accountService.fillAccounts();
        System.out.println("Insert your card, please.");
        return validateCard(br);
    }

    public boolean checkByBankCardPattern(String inputCardNumber) {
        Pattern cardNumberPattern = Pattern.compile("([2-6]([0-9]{3}))(([0-9]{4}){3})");
        Matcher cardNumberMatcher = cardNumberPattern.matcher(inputCardNumber);
        return cardNumberMatcher.matches();
    }

    public String checkInputCardNumber(BufferedReader br) {
        System.out.println("Waiting..");
        StringBuilder sb = new StringBuilder();
        boolean isRead = false;
        String inputCardNumber = "";
        System.out.println("Enter your card number, please, or enter EXIT to return your card:");
        while (!isRead) {
            try {
                inputCardNumber = br.readLine();
                if (!inputCardNumber.equalsIgnoreCase("EXIT")) {
                    if (checkByBankCardPattern(inputCardNumber)) {
                        sb.append(inputCardNumber, 0, 4).append("-").append(inputCardNumber, 4, 8).append("-")
                                .append(inputCardNumber, 8, 12).append("-").append(inputCardNumber.substring(12));
                        isRead = true;
                        return sb.toString();
                    } else {
                        System.out.println("Input error, try again or enter EXIT to return your card:");
                    }
                } else {
                    isRead = true;
                }
            } catch (IOException | NumberFormatException e) {
                System.out.println("Input error, try again or enter EXIT to return your card:");
            }
        }
        return inputCardNumber;
    }

    public Card checkInputPin(BufferedReader br, Card card) {
        boolean isRead = false;
        int pinInputTry = 3;
        System.out.println("Enter your pin, please, or enter EXIT to return your card.");
        while (!isRead) {
            if (pinInputTry != 0) {
                try {
                    System.out.println("Number of attempts: " + pinInputTry);
                    String inputPin = br.readLine();
                    if (!inputPin.equalsIgnoreCase("EXIT")) {
                        if (inputPin.equals(card.getPin())) {
                            if (card.getStatus().equals(CardStatus.BLOCKED)) {
                                System.out.println("Your card is still blocked. Contact your bank.");
                            }
                            isRead = true;
                        } else {
                            pinInputTry--;
                            if (pinInputTry == 0) {
                                System.out.println("Wrong pin.");
                            } else {
                                System.out.println("Wrong pin, try again or enter EXIT to return your card:");
                            }
                        }
                    } else {
                        isRead = true;
                        card = null;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                card.setStatus(CardStatus.BLOCKED);
                System.out.println("Card is blocked. Contact your bank.");
                isRead = true;
                accountService.writeAccounts();
            }
        }
        return card;
    }

    public Card validateCard(BufferedReader br) {
        String inputNumber = checkInputCardNumber(br);
        Card card = null;
        if (!inputNumber.equalsIgnoreCase("exit")) {
            card = checkInputPin(br, accountService.findCardByCardNumber(inputNumber));
        }
        return card;
    }
}







