public class Atm {
    private final static AccountDao accountDao = new AccountDao();
    private final static AccountService accountService = new AccountService(accountDao);
    private final static CardValidator cardValidator = new CardValidator(accountService);
    private final static Terminal terminal = new Terminal(accountService);

    public static void main(String[] args) {
        AtmStarter atmStarter = new AtmStarter(cardValidator, terminal);
        atmStarter.start();
    }
}
