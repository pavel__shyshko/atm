import java.util.ArrayList;
import java.util.List;

public class AccountDao {
    private List<Account> accounts = new ArrayList();

    public void fillAccountDao(List<Account> accountList) {
        this.accounts = accountList;
    }

    public List<Account> getAllAccounts() {
        return accounts;
    }

    public Account getAccountByCard(Card card) {
        return accounts.stream().filter(ac -> ac.getCard().equals(card)).findAny().get();
    }
}
