public class Card {
    private String number;
    private String pin;
    private CardStatus status;

    public Card(String number, String pin, CardStatus status) {
        this.number = number;
        this.pin = pin;
        this.status = status;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public CardStatus getStatus() {
        return status;
    }

    public void setStatus(CardStatus status) {
        this.status = status;
    }
}
