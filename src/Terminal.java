import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Terminal {
    private AccountService accountService;
    private static long atmTotalCash;

    public Terminal(AccountService accountService) {
        this.accountService = accountService;
    }

    public void startProcessingUserRequests(BufferedReader br, Card card) {
        if ((card != null) && (card.getStatus().equals(CardStatus.ACTIVE))) {
            initializeAtmCash();
            selectOperation(br, card);
        } else {
            System.out.println("Take your card, please");
        }
    }

    public void writeTotalAtmCash(long atmTotalCash) {
        List<String> lines = new ArrayList<>();
        lines.add(Long.toString(atmTotalCash));
        try {
            Files.write(Paths.get("atm-cash.txt"), lines, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initializeAtmCash() {
        try {
            atmTotalCash = Long.parseLong(Files.readAllLines(Paths.get("atm-cash.txt")).get(0));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void selectOperation(BufferedReader br, Card card) {
        if (card.getStatus().equals(CardStatus.ACTIVE)) {
            boolean isRead = false;
            while (!isRead) {
                System.out.println("To select an operation, enter one of the following numbers:");
                System.out.println("1 - Check balance\n2 - Cash advance\n3 - Cash acceptance\n0 - Complete the work\n");
                try {
                    int number = Integer.parseInt(br.readLine());
                    switch (number) {
                        case 1 -> accountService.checkBalance(card);
                        case 2 -> atmTotalCash = accountService.getMoneyFromAtm(br, card, atmTotalCash);
                        case 3 -> atmTotalCash = accountService.addMoneyToAtm(br, card, atmTotalCash);
                        case 0 -> {
                            isRead = true;
                            System.out.println("Take your card, please");
                            accountService.writeAccounts();
                            writeTotalAtmCash(atmTotalCash);
                        }
                        default -> System.out.println("Incorrect input. To select an operation, enter one of the following numbers: 1, 2, 3, 0.\n");
                    }
                } catch (IOException | NumberFormatException e) {
                    System.out.println("Incorrect input. To select an operation, enter one of the following numbers: 1, 2, 3, 0.\n");
                }
            }
        } else {
            System.out.println("Take your card, please.");
        }
    }
}
